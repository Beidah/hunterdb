cmake_minimum_required(VERSION 3.3)
project(hunterDB)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_FILES_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/build${CMAKE_FILES_DIRECTORY})
set(PROJECT_SOURCE_DIR ${PROJECT_SOURCE_DIR}/src)

set (SRC_FILES ${PROJECT_SOURCE_DIR}/Application.cpp
               ${PROJECT_SOURCE_DIR}/InputBuffer.cpp
               ${PROJECT_SOURCE_DIR}/Table.cpp
               ${PROJECT_SOURCE_DIR}/Page.cpp
               ${PROJECT_SOURCE_DIR}/Statement.cpp)

add_executable(db ${SRC_FILES})