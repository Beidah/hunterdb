#pragma once

#include "Page.hpp"

class Table;

class Cursor {
private:
  Table* m_Table;
  u_size m_RowNumber;
  bool m_EndOfTable;
public:
  Cursor(Table* table, u_size rowNumber);

  Row* Value();
  void Advance();
};

class Table
{
private:
  u_size m_NumberOfRows;
  Pager* m_Pager;

  friend class Cursor;
public:

  // Initialization and closing
  void OpenDatabase(const char* filePath);
  void CloseDatabase();

  // Properties
  bool Full() const { return m_NumberOfRows >= TABLE_MAX_ROWS; }
  u_size GetNumberOfRows() const { return m_NumberOfRows; }

  // Rows
  void AddRow(Row row);
  Row GetRow(u_size rowNumber) { return retrieveRow(rowNumber); };

  Cursor* Start() { return new Cursor(this, 0); };
  Cursor* End() { return new Cursor(this, m_NumberOfRows); };

private:
  Row *retrieveRowPtr(u_size rowNumber);
  Row retrieveRow(u_size rowNumber);
};