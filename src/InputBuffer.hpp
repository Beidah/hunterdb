#pragma once

#include <string>

#include "Statement.hpp"

enum class MetaCommandResult {
    Success, Unreconized
};

enum class PrepareResult {
    Success, Unreconized, SyntaxError, StringTooLong, NegativeId
};

class InputBuffer {
private:
    std::string m_Buffer;
public:
    void ReadInput();

    std::string GetBuffer() const { return m_Buffer; }

    MetaCommandResult MetaCommand(Table* table);
    PrepareResult PrepareStatement(Statement& statement);

private:
    PrepareResult prepareInsert(Statement& statement);
};