#pragma once

#include <stdint.h>

typedef uint32_t u_size;

const u_size COLUMN_USERNAME_SIZE = 32;
const u_size COLUMN_EMAIL_SIZE = 255;

struct Row
{
    int id;
    char username[COLUMN_USERNAME_SIZE + 1];
    char email[COLUMN_EMAIL_SIZE + 1];

    void Print();
    void Serialize(Row *destination);
};

#define size_of_attribute(Struct, Attribute) sizeof(((Struct *)0)->Attribute)

const u_size ID_SIZE = size_of_attribute(Row, id);
const u_size USERNAME_SIZE = size_of_attribute(Row, username);
const u_size EMAIL_SIZE = size_of_attribute(Row, email);
const u_size ID_OFFSET = 0;
const u_size USERNAME_OFFSET = ID_OFFSET + ID_SIZE;
const u_size EMAIL_OFFSET = USERNAME_OFFSET + USERNAME_SIZE;
const u_size ROW_SIZE = ID_SIZE + USERNAME_SIZE + EMAIL_SIZE;

const u_size PAGE_SIZE = 4096;
const u_size TABLE_MAX_PAGES = 100;
const u_size ROWS_PER_PAGE = PAGE_SIZE / ROW_SIZE;
const u_size TABLE_MAX_ROWS = ROWS_PER_PAGE * TABLE_MAX_PAGES;

struct Page
{
    Row rows[ROWS_PER_PAGE];
};

class Pager
{
  private:
    int m_FileDescriptor;
    u_size m_FileLength;
    Page *m_Pages[TABLE_MAX_PAGES];

  public:
    Pager() = delete;

    Pager(const char *filePath);
    ~Pager();

    u_size GetFileLength() const { return m_FileLength; }
    Page *GetPage(u_size pageNumber);

    void Flush(u_size pageNumber, size_t size);
};