#pragma once

#include "Table.hpp"

enum class StatementType {
    Insert, Select,
};

enum class ExecuteResult {
    Success, TableFull, Unreconized
};

class Statement {
private:
    StatementType m_Type;
    Row m_Row;
public:
    Statement() = default;
    Statement(StatementType type) : m_Type(type) {}
    Statement(StatementType type, Row row) : m_Type(type), m_Row(row) {}

    ExecuteResult Execute(Table* table);

private:
    ExecuteResult executeInsert(Table* table);
    ExecuteResult executeSelect(Table* table);
};
