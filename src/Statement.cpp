#include "Statement.hpp"
#include <cstdio>

ExecuteResult Statement::Execute(Table *table)
{
    switch (m_Type)
    {
    case StatementType::Insert:
        return executeInsert(table);
    case StatementType::Select:
        return executeSelect(table);
    default:
        return ExecuteResult::Unreconized;
    }

}

ExecuteResult Statement::executeInsert(Table *table)
{
    if (table->Full())
        return ExecuteResult::TableFull;

    table->AddRow(m_Row);
    return ExecuteResult::Success;
}

ExecuteResult Statement::executeSelect(Table *table)
{
    for (u_size i = 0; i < table->GetNumberOfRows(); i++)
    {
        Row row = table->GetRow(i);
        row.Print();
    }

    return ExecuteResult::Success;
}
