#include <cstdlib>

#include "InputBuffer.hpp"
#include "Table.hpp"

void PrintPrompt()
{
    printf("db > ");
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Must supply a database filename.\n");
		exit(EXIT_FAILURE);
    }

    char* fileName = argv[1];

    Table *table = new Table();
    table->OpenDatabase(fileName);
    InputBuffer *inputBuffer = new InputBuffer();

    while (true)
    {
        PrintPrompt();
        inputBuffer->ReadInput();
        std::string command = inputBuffer->GetBuffer();

        if (!command.empty() && command.front() == '.')
        {
            switch (inputBuffer->MetaCommand(table))
            {
            case MetaCommandResult::Success:
                continue;
            case MetaCommandResult::Unreconized:
                printf("Unreconized command '%s'\n", command.c_str());
                continue;
            }
        }

        Statement statement;
        switch (inputBuffer->PrepareStatement(statement))
        {
            break;
        case PrepareResult::Unreconized:
            printf("Unrecognized keyword at start of '%s'.\n", command.c_str());
            continue;
        case PrepareResult::SyntaxError:
            printf("Syntax error. Could not parse statement.\n");
            continue;
        case PrepareResult::StringTooLong:
            printf("String is too long.\n");
            continue;
        case PrepareResult::NegativeId:
            printf("ID must be positive.\n");
            continue;
        }

        switch (statement.Execute(table))
        {
        case ExecuteResult::Success:
            printf("Executed.\n");
            break;
        case ExecuteResult::TableFull:
            printf("Error: Table full.\n");
            break;
        }
    }
}
